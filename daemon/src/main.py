import time

from state import State
from pipes import SDPipes

state = State.init_from_config_file('config.json')

pipes = SDPipes()

# Main Loop
i = 0
while True:
    print(f"[NOTE] --- Iter {i}")
    start = time.time()
    # Merge values
    state.merge_values()
    state.start_job()
    # Run text to image
    img = pipes.text_to_image(state)
    file_prefix = state.end_job()
    img.save(f"{state.outputs_root}/{file_prefix}.{state.image_ext}")
    state.save_values(f"{state.outputs_root}/{file_prefix}.json")
    # Prepare for next iteration
    end = time.time()
    print(f"[INFO] --- Iter {i}: elapsed: {end - start} sec")
    i += 1
