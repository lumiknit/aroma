import datetime
import json

def merge_dict(dst, src):
    for k in src:
        if k in dst and isinstance(dst[k], dict):
            merge_dict(dst[k], src[k])
        else:
            dst[k] = src[k]

class State:
    """
    Global configuration set for daemon
    """

    def __init__(self, d):
        # Get roots
        self.models_root = d['models_root']
        self.state_root = d['state_root']
        self.outputs_root = d['outputs_root']
        self.image_ext = d['image_ext']

        self.values = d['init_values']

    @classmethod
    def init_from_config_file(cls, path):
        with open(path, 'r') as f:
            return cls(json.load(f))
        
    def merge_from_file(self, path):
        # Read file
        with open(path, 'r') as f:
            # Read contents
            contents = f.read()
        with open(path, 'w') as f:
            # Truncate
            f.write('{}')
        # Merge
        try:
            merge_dict(self.values, json.loads(contents))
        except Exception as e:
            print("[WARN] Failed to merge dict, do nothing")
            print(e)
    
    def merge_values(self):
        return self.merge_from_file(f"{self.state_root}/values.json")

    def save_values(self, path):
        with open(path, 'w') as f:
            f.write(json.dumps(self.values))

    def start_job(self):
        # Set start time
        self.job = {
            'start_time': datetime.datetime.now().isoformat(),
            'values': self.values,
        }
        # Write to file
        with open(f"{self.state_root}/current_job.json", 'w') as f:
            f.write(json.dumps(self.job))
    
    def end_job(self):
        # Set end time
        now = datetime.datetime.now()
        self.job['end_time'] = now.isoformat()
        file_prefix = now.strftime("%y%m%d-%H%M%S-%f")
        self.job['filename'] = f"{file_prefix}.{self.image_ext}"
        # Write to file
        with open(f"{self.state_root}/last_job.json", 'w') as f:
            f.write(json.dumps(self.job))
        # Reset
        self.job = None
        # Return file prefix
        return file_prefix
    
    def write_state(self, name, values):
        with open(f"{self.state_root}/state.json", 'w') as f:
            f.write(json.dumps({
                'name': name,
                'values': values,
            }))