$(() => {
  // Read query param
  const urlParams = new URLSearchParams(window.location.search);
  const src = urlParams.get('img');
  $('#img').attr('src', '/aroma-static/outputs/' + src)

  $(document).on('click', () => {
    // Close window
    window.close();
  });
});