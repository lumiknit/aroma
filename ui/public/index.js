const appendAlert = (type, message) => {
  let html = `
    <div class="alert alert-` + type + ` alert-dismissible mt-2 fade show" role="alert">
      ` + message + `
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
  `;
  $('#alert-placeholder').append(html);
};

let images = [];
let image_map = {};

const createCard = (imgName) => {
  let name = imgName.split(".")[0];
  let source = "/aroma-static/outputs/" + imgName;
  let target = "/static/imgview.html?img=" + encodeURIComponent(imgName);
  let html = `
    <div id="card--` + name + `" class="card mt-2">
      <button type="button" class="card-header bg-secondary btn btn-sm btn-secondary p-1" data-bs-toggle="collapse" data-bs-target="#card-collapse--` + name + `" aria-expanded="false" aria-controls="card-collapse--` + name + `" title="Click to open details"> <div class="text-truncate" style="font-size: 0.5em;">` + name + `</div> </button>
      <a href="` + target + `" target="_blank">
        <img loading="lazy" src="` + source + `" class="card-img-top">
      </a>
      
      <div class="collapse" id="card-collapse--` + name + `">
        <div class="card-body">
          <button class="btn btn-sm btn-outline-danger" onclick="deleteImage('` + imgName + `')">Delete</button>
          <p class="font-monospace" style="font-size: 0.75em;">
            <small id="card-desc--` + name + `">
              <button class="button btn-sm btn-outline-secondary" onclick="loadImageSettings('` + imgName + `')"> Settings </button>
            </small>
          </p>
        </div>
      </div>
    </div>
  `;
  return html;
};

const loadImageSettings = (imgName) => {
  const name = imgName.split(".")[0];
  console.log(image_map);
  if(image_map[imgName] === undefined) {
    console.error("Cannot find image settings: " + name);
    return;
  }
  // Get desc
  let desc = $("#card-desc--" + name);
  desc.text("Loading...");
  // Read settings
  $.ajax({
    url: "/aroma-static/outputs/" + name + ".json",
    type: "GET",
    dataType: "text",
    async: true,
    success: (data) => {
      image_map[imgName].config = JSON.parse(data);
      desc.text(data);
      desc.prepend($(`<button class="button btn-sm btn-outline-primary" onclick="reuseImageSettings('` + imgName + `')"> Reuse </button>`));
    },
    error: (xhr, status, error) => {
      desc.text("Error: " + error);
    }
  });
};

const reuseImageSettings = (imgName) => {
  let config = image_map[imgName];
  if(config === undefined) {
    console.error("Cannot find image settings: " + name);
    return;
  }
  // Set config
  $('#config-model-path').val(config.config.model.path);
  $('#config-sampling-steps').val(config.config.params.sampling_steps);
  $('#config-cfg-scale').val(config.config.params.cfg_scale);
  $('#config-width').val(config.config.params.width);
  $('#config-height').val(config.config.params.height);
  $('#config-prompt').val(config.config.params.prompt);
  $('#config-negative-prompt').val(config.config.params.negative_prompt);
  let cloned = JSON.parse(JSON.stringify(config.config.params));
  delete cloned.sampling_steps;
  delete cloned.cfg_scale;
  delete cloned.width;
  delete cloned.height;
  delete cloned.prompt;
  delete cloned.negative_prompt;
  $('#config-other').val(JSON.stringify(cloned));
  // Make alert
  appendAlert("success", "Settings loaded!");
  // Go to top
  window.scrollTo(0, 0);
};

const deleteImage = (imgName) => {
  // Get card
  let name = imgName.split(".")[0];
  let card = $("#card--" + name);
  // Create dialog to confirm deletion
  if(confirm("Delete image and settings of the file named: " + name)) {
    // Send delete request
    $.ajax({
      url: "/api/outputs/" + imgName,
      type: "DELETE",
      async: true,
      success: (data) => {
        // Remove card
        card.remove();
        // Remove from images
        images = images.filter((n) => {
          return imgName != n;
        });
        delete image_map[name];
      }
    });
  }
};

const galleryColumn = (index) => {
  if(index < 0 || index > 2) {
    console.error("[ERROR] Invalid index: " + index);
    return null;
  }
  return $("#gallery-col-" + index);
};

const resetColumn = (index) => {
  let column = galleryColumn(index);
  column.empty();
};

const imageNameRegExp = new RegExp("^[a-zA-Z0-9_\\-\\.]+$");
const pushImage = (imgName) => {
  // Check if image is already in gallery
  if(image_map[imgName] !== undefined) {
    return false;
  }
  // Check image name is valid
  if(typeof imgName !== "string" || !imageNameRegExp.test(imgName)) {
    console.error("[ERROR] Invalid image name: " + imgName);
    return false;
  }
  // Find column with least cards
  let minCol = 0;
  for(var i = 1; i < 3; i++) {
    let col_i = galleryColumn(i);
    let col_m = galleryColumn(minCol);
    if(col_i.height() < col_m.height() || (col_i.height() == col_m.height() && col_i.children().length < col_m.children().length)) {
      minCol = i;
    }
  }
  // Push card to column
  let html = createCard(imgName);
  let column = galleryColumn(minCol);
  column.prepend(html);
  // Push to images
  images.push(imgName);
  image_map[imgName] = {};
  return true;
};

const reload = () => {
  // Load current state
  $.get("/aroma-static/state/state.json", (data) => {
    let status = $('#state-status');
    status.text(data.name);
    const statusColor = {
      "load_model": "danger",
      "update_prompt": "warning",
      "txt2img": "primary",
      "highres_fix": "primary",
      "img2img": "primary",
      "save_image": "info",
    };
    status.removeClass("bg-danger bg-warning bg-primary bg-info");
    status.addClass("bg-" + statusColor[data.name]);
    $('#state-details').text(JSON.stringify(data.values));
    // Check it is progress
    let progress = $('#state-progress');
    if(typeof data.values.step === "number"
        && typeof data.values.total_steps === "number") {
      // Update progress bar
      percentage = 100 * data.values.step / data.values.total_steps;
      text = "" + data.values.step + "/" + data.values.total_steps;
      progress.addClass("progress-bar-striped progress-bar-animated");
      progress.css("width", percentage + "%").text(text);
    } else {
      // Set to full progress
      progress.removeClass("progress-bar-striped progress-bar-animated");
      progress
        .css("width", "100%")
        .text("--/--");
    }
  });
  // Load current job
  $.get("/aroma-static/state/current_job.json", (data) => {
    let start_time = new Date(data.start_time);
    let elapsed = new Date() - start_time;
    $('#state-started').text(start_time.toLocaleString());
    $('#state-elapsed').text((elapsed / 1000).toFixed(1) + "s");

    // Load default values
    if(data.values !== undefined) {
      if(data.values.model !== undefined) {
        $('#config-model-path').attr("placeholder", data.values.model.path);
      }
      if(data.values.params !== undefined) {
        $('#config-sampling-steps').attr("placeholder", data.values.params.sampling_steps);
        $('#config-cfg-scale').attr("placeholder", data.values.params.cfg_scale);
        $('#config-width').attr("placeholder", data.values.params.width);
        $('#config-height').attr("placeholder", data.values.params.height);
        $('#config-prompt').attr("placeholder", data.values.params.prompt);
        $('#config-negative-prompt').attr("placeholder", data.values.params.negative_prompt);
        let cloned = JSON.parse(JSON.stringify(data.values.params));
        delete cloned.sampling_steps;
        delete cloned.cfg_scale;
        delete cloned.width;
        delete cloned.height;
        delete cloned.prompt;
        delete cloned.negative_prompt;
        $('#config-other').attr("placeholder", JSON.stringify(cloned));
      }
    }
  });
  // Load last job
  $.get("/aroma-static/state/last_job.json", (data) => {
    let start_time = new Date(data.start_time);
    let end_time = new Date(data.end_time);
    let elapsed = end_time - start_time;
    $('#state-last-elapsed').text((elapsed / 1000).toFixed(1) + "s");
    let filename = data.filename;
    $('#state-last-file').text(filename);
    pushImage(filename);
  });
};

const loadAllGallery = () => {
  for(var i = 0; i < 3; i++) {
    resetColumn(i);
  }
  images = [];
  image_map = {};

  $.get("/api/outputs", (data) => {
    data.forEach((imgName) => {
      pushImage(imgName);
    });
  });
};

const setModel = (model) => {
  $('#config-model-path').val(model);
};

const loadAllModels = () => {
  $.get("/api/models", (data) => {
    console.log(data);
    // Set dropdown
    let list = $('#models-dropdown-list');
    list.text('');
    data.sort().forEach((model) => {
      list.append($(`<li><a class="dropdown-item" href="#" onclick="setModel('` + model + `')">` + model + `</a></li>`));
    });
  });
};

const archiveOutputs = () => {
  // Make confirm
  if(!confirm("Warning: It will archive all outputs as .tar.gz and DELETE ALL FILES IN OUTPUTS directory.")) {
    return;
  }
  // Send request
  $.ajax({
    url: "/api/outputs/archive",
    type: "POST",
    async: true,
    success: (data) => {
      appendAlert("success", "Archived! Please refresh the current page!");
    },
    error: (xhr, status, error) => {
      appendAlert("danger", "Archive request failed: " + error);
    }
  });
};

const applyConfig = () => {
  // Read other config
  let values = {
    model: {},
    params: {}
  };
  try {
    let text = $('#config-other').val().trim();
    if(text.length > 0) {
      values.params = JSON.parse($('#config-other').val());
    }
  } catch(e) {
    appendAlert("danger", "Invalid other config: " + e);
    return;
  }
  // Read sampling steps
  let ss = $('#config-sampling-steps').val().trim();
  if(ss.length > 0) {
    let sampling_steps = parseInt($('#config-sampling-steps').val());
    if(isNaN(sampling_steps) || sampling_steps < 1 || sampling_steps > 1000) {
      appendAlert("danger", "Invalid sampling steps: " + ss);
      return;
    }
    values.params.sampling_steps = sampling_steps;
  }
  // Read cfg scale
  let cs = $('#config-cfg-scale').val().trim();
  if(cs.length > 0) {
    let cfg_scale = parseFloat($('#config-cfg-scale').val());
    if(isNaN(cfg_scale) || cfg_scale < 0.1 || cfg_scale > 20) {
      appendAlert("danger", "Invalid cfg scale: " + cs);
      return;
    }
    values.params.cfg_scale = cfg_scale;
  }
  // Read width
  let w = $('#config-width').val().trim();
  if(w.length > 0) {
    let width = parseInt($('#config-width').val());
    if(isNaN(width) || width < 1 || width > 10000) {
      appendAlert("danger", "Invalid width: " + w);
      return;
    }
    values.params.width = width;
  }
  // Read height
  let h = $('#config-height').val().trim();
  if(h.length > 0) {
    let height = parseInt($('#config-height').val());
    if(isNaN(height) || height < 1 || height > 10000) {
      appendAlert("danger", "Invalid height: " + h);
      return;
    }
    values.params.height = height;
  }
  // Read prompt
  let p = $('#config-prompt').val().trim();
  if(p.length > 0) {
    values.params.prompt = p;
  }
  // Read negative prompt
  let np = $('#config-negative-prompt').val().trim();
  if(np.length > 0) {
    values.params.negative_prompt = np;
  }
  // Read model path
  let mp = $('#config-model-path').val().trim();
  if(mp.length > 0) {
    values.model.path = mp;
  }
  // Send request
  $.ajax({
    url: "/api/values",
    type: "PUT",
    data: JSON.stringify(values),
    contentType: "application/json",
    async: true,
    success: (data) => {
      appendAlert("success", "Updated!");
      // Reset all fields
      $('#config-model-path').val("");
      $('#config-sampling-steps').val("");
      $('#config-cfg-scale').val("");
      $('#config-width').val("");
      $('#config-height').val("");
      $('#config-prompt').val("");
      $('#config-negative-prompt').val("");
      $('#config-other').val("");
    },
    error: (xhr, status, error) => {
      appendAlert("danger", "Update request failed: " + error);
    }
  });
};

// Daemon APIs
const updateDaemonStatus = () => {
  let btnStatus = $('#btn-daemon-status');
  let btnSwitch = $('#btn-daemon-switch');
  // Call API
  $.get("/api/daemon", (data) => {
    if(data === "running") {
      btnStatus.removeClass("btn-outline-danger");
      btnStatus.addClass("btn-outline-success");
      btnSwitch.removeClass("btn-danger");
      btnSwitch.addClass("btn-success");
      btnSwitch.text("Off");
    } else {
      btnStatus.removeClass("btn-outline-success");
      btnStatus.addClass("btn-outline-danger");
      btnSwitch.removeClass("btn-success");
      btnSwitch.addClass("btn-danger");
      btnSwitch.text("On");
    }
  });
};

const switchDaemonStatus = () => {
  // Toggle daemon status
  if(confirm("Switch daemon status?")) {
    $.ajax({
      url: "/api/daemon",
      type: "PUT",
      async: true,
      success: (data) => {
        updateDaemonStatus();
      },
      error: (xhr, status, error) => {
        appendAlert("danger", "Daemon switch failed: " + error);
      }
    });
  }
};

// Init
$(() => {
  // Set prompt loader
  $('#config-prompt').on('focus', () => {
    // If itself is empty, fill by placeholder
    c = $('#config-prompt')
    if(c.val() == "") {
      c.val(c.attr("placeholder"));
    }
  });
  $('#config-negative-prompt').on('focus', () => {
    // If itself is empty, fill by placeholder
    c = $('#config-negative-prompt')
    if(c.val() == "") {
      c.val(c.attr("placeholder"));
    }
  });
  $('#config-other').on('focus', () => {
    // If itself is empty, fill by placeholder
    c = $('#config-other')
    if(c.val() == "") {
      c.val(c.attr("placeholder"));
    }
  });
  // Load models
  loadAllModels();
  // Load gallery
  loadAllGallery();
  // Create interval to reload gallery
  setInterval(() => {
    reload();
    updateDaemonStatus();
  }, 517);
});
